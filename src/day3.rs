use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day3)]
pub fn generator_part1(input: &str) -> Vec<String> {
    input.lines().map(|string| string.to_string()).collect()
}

#[aoc(day3, part1)]
pub fn solve_part1(input: &[String]) -> usize {
    //dbg!(&input);
    let mut gamma_rate_vec: Vec<u32> = Vec::with_capacity(input[0].len());
    let mut delta_rate_vec: Vec<u32> = Vec::with_capacity(input[0].len());
    let mut binary_count: Vec<(u32, u32)> = vec![(0, 0); input[0].len()];

    for line in input {
        line.char_indices().for_each(|character| match character {
            (i, '0') => binary_count[i].0 += 1,
            (i, '1') => binary_count[i].1 += 1,
            (_, _) => panic!(),
        })
    }

    // gamma_rate: most common bit
    // delta_rate: least common bit
    for pair in binary_count {
        if pair.0 > pair.1 {
            gamma_rate_vec.push(0);
            delta_rate_vec.push(1);
        } else {
            gamma_rate_vec.push(1);
            delta_rate_vec.push(0);
        }
    }

    //dbg!(&gamma_rate_vec);
    let gamma_rate_string = reduce_into_string(&mut gamma_rate_vec);
    //dbg!(&gamma_rate_string);
    let gamma_rate = usize::from_str_radix(&gamma_rate_string, 2).unwrap();

    let delta_rate_string = reduce_into_string(&mut delta_rate_vec);
    let delta_rate = usize::from_str_radix(&delta_rate_string, 2).unwrap();

    gamma_rate * delta_rate
}

pub fn reduce_into_string(gamma_rate_vec: &mut Vec<u32>) -> String {
    let gamma_rate_string = gamma_rate_vec
        .clone()
        .iter()
        .fold("".to_string(), |mut acc, x| {
            acc.push(std::char::from_digit(*x, 10).unwrap());
            acc
        });
    gamma_rate_string
}

#[aoc(day3, part2)]
pub fn solve_part2(input: &[String]) -> usize {
    let mut oxygen_vec: Vec<&str> = <&[std::string::String]>::clone(&input)
        .iter()
        .map(|elem| elem.as_str())
        .collect();
    let mut i = 0;
    while oxygen_vec.len() > 1 {
        oxygen_vec = reduce_by_one_most_common(&mut oxygen_vec, i);
        i += 1;
    }

    let mut co2_vec: Vec<&str> = <&[std::string::String]>::clone(&input)
        .iter()
        .map(|elem| elem.as_str())
        .collect();
    let mut i = 0;
    while co2_vec.len() > 1 {
        co2_vec = reduce_by_one_least_common(&mut co2_vec, i);
        i += 1;
    }

    // most common
    let oxygen_gen_rating = usize::from_str_radix(oxygen_vec[0], 2).unwrap();
    // least common
    let co2_scrub_rating = usize::from_str_radix(co2_vec[0], 2).unwrap();

    dbg!(&oxygen_gen_rating);
    dbg!(&co2_scrub_rating);

    oxygen_gen_rating * co2_scrub_rating
}

pub fn reduce_by_one_most_common<'a>(vec: &mut [&'a str], index: usize) -> Vec<&'a str> {
    let mut pair = (0, 0);
    vec.iter().for_each(|elem| match &elem[index..index + 1] {
        "0" => pair.0 += 1,
        "1" => pair.1 += 1,
        _ => panic!(),
    });
    vec.iter_mut()
        .filter(|elem| {
            if pair.0 > pair.1 {
                &elem[index..index + 1] == "0"
            } else {
                &elem[index..index + 1] == "1"
            }
        })
        .map(|elem| *elem)
        .collect()
}

pub fn reduce_by_one_least_common<'a>(vec: &mut [&'a str], index: usize) -> Vec<&'a str> {
    let mut pair = (0, 0);
    vec.iter().for_each(|elem| match &elem[index..index + 1] {
        "0" => pair.0 += 1,
        "1" => pair.1 += 1,
        _ => panic!(),
    });
    vec.iter_mut()
        .filter(|elem| {
            if pair.0 <= pair.1 {
                &elem[index..index + 1] == "0"
            } else {
                &elem[index..index + 1] == "1"
            }
        })
        .map(|elem| *elem)
        .collect()
}

#[cfg(test)]
mod tests {
    use crate::day3::{
        generator_part1, reduce_by_one_least_common, reduce_by_one_most_common, solve_part1,
        solve_part2,
    };

    #[test]
    fn test01() {
        let input = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";

        assert_eq!(solve_part1(&*generator_part1(input)), 198)
    }

    #[test]
    fn test02() {
        let input = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";

        assert_eq!(solve_part2(&*generator_part1(input)), 230)
    }

    #[test]
    fn that_most_common_reducing_works() {
        let input = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";
        let new_input = generator_part1(input);
        let mut oxygen_vec: Vec<&str> = <&[std::string::String]>::clone(&&*new_input)
            .iter()
            .map(|elem| elem.as_str())
            .collect();
        let mut i = 0;
        while oxygen_vec.len() > 1 {
            dbg!(&oxygen_vec);
            oxygen_vec = reduce_by_one_most_common(&mut oxygen_vec, i);
            i += 1;
        }
        assert_eq!(oxygen_vec[0], "10111");
    }

    #[test]
    fn that_least_common_reducing_works() {
        let input = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";
        let new_input = generator_part1(input);
        let mut oxygen_vec: Vec<&str> = <&[std::string::String]>::clone(&&*new_input)
            .iter()
            .map(|elem| elem.as_str())
            .collect();
        let mut i = 0;
        while oxygen_vec.len() > 1 {
            dbg!(&oxygen_vec);
            oxygen_vec = reduce_by_one_least_common(&mut oxygen_vec, i);
            i += 1;
        }
        assert_eq!(oxygen_vec[0], "01010");
    }

    #[test]
    fn that_i_am_not_going_mad_over_binary_conversion() {
        let co2_scrub_rating = usize::from_str_radix("01010", 2).unwrap();
        assert_eq!(co2_scrub_rating, 10);
    }

    #[test]
    fn that_i_am_not_going_mad_over_binary_conversion_number_two() {
        let oxygen_rating = usize::from_str_radix("10111", 2).unwrap();
        assert_eq!(oxygen_rating, 23);
    }
}
