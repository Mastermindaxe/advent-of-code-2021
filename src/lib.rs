#![feature(int_abs_diff)]
#![feature(trusted_random_access)]

use aoc_runner_derive::aoc_lib;

pub mod day1;
pub mod day10;
pub mod day13;
pub mod day2;
pub mod day3;
pub mod day6;
pub mod day7;
pub mod day8;
pub mod day9;

aoc_lib! { year = 2021 }
