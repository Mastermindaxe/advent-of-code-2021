use aoc_runner_derive::{aoc, aoc_generator};

use crate::day2::Movement::{DOWN, FORWARD, UP};

pub enum Movement {
    FORWARD(usize),
    DOWN(usize),
    UP(usize),
}

#[aoc_generator(day2)]
pub fn generator_part1(input: &str) -> Vec<Movement> {
    input
        .lines()
        .map(|line| {
            if line.contains("forward") {
                return FORWARD(line.split(' ').nth(1).unwrap().parse().unwrap());
            } else if line.contains("down") {
                return DOWN(line.split(' ').nth(1).unwrap().parse().unwrap());
            } else if line.contains("up") {
                return UP(line.split(' ').nth(1).unwrap().parse().unwrap());
            } else {
                unreachable!()
            }
        })
        .collect()
}

#[aoc(day2, part1)]
pub fn solve_part1(input: &[Movement]) -> usize {
    let mut horizontal_pos = 0;
    let mut depth = 0;

    for i in input {
        match i {
            FORWARD(x) => horizontal_pos += x,
            DOWN(x) => depth += x,
            UP(x) => depth -= x,
        }
    }

    horizontal_pos * depth
}

#[aoc(day2, part2)]
pub fn solve_part2(input: &[Movement]) -> usize {
    let mut horizontal_pos = 0;
    let mut depth = 0;
    let mut aim = 0;

    for i in input {
        match i {
            FORWARD(x) => {
                horizontal_pos += x;
                depth += aim * x;
            }
            DOWN(x) => aim += x,
            UP(x) => aim -= x,
        }
    }

    horizontal_pos * depth
}

mod tests {

    #[test]
    fn test01() {
        use crate::day2::{generator_part1, solve_part1};

        assert_eq!(
            solve_part1(&generator_part1(
                "forward 5
down 5
forward 8
up 3
down 8
forward 2"
            )),
            150
        );
    }

    #[test]
    fn test02() {
        use crate::day2::{generator_part1, solve_part2};

        assert_eq!(
            solve_part2(&generator_part1(
                "forward 5
down 5
forward 8
up 3
down 8
forward 2"
            )),
            900
        );
    }
}
