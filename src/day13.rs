use crate::day13::PaperFoldInstruction::{FoldAlongX, FoldAlongY};
use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::HashSet;

// Code yoinked from https://github.com/ChaiTRex/advent-of-code-2021/blob/main/rust/day13/src/main.rs
// and modified after trying for an hour and giving up on weird datatypes :(

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum PaperFoldInstruction {
    FoldAlongX(u16),
    FoldAlongY(u16),
}

#[aoc_generator(day13)]
pub fn generator_part1(input: &str) -> (HashSet<(u16, u16)>, Vec<PaperFoldInstruction>) {
    let (dots, folds) = input.split_once("\n\n").unwrap();
    let dots = dots
        .lines()
        .map(|line| {
            let (x, y) = line.split_once(',').unwrap();
            (x.parse::<u16>().unwrap(), y.parse::<u16>().unwrap())
        })
        .collect::<HashSet<_>>();

    let folds = folds
        .lines()
        .map(|line| line.parse::<PaperFoldInstruction>().unwrap())
        .collect::<Vec<_>>();

    (dots, folds)
}

#[aoc(day13, part1)]
pub fn solve_part1(input: &(HashSet<(u16, u16)>, Vec<PaperFoldInstruction>)) -> usize {
    input.1[0].apply(input.0.clone()).len()
}

#[aoc(day13, part2)]
pub fn solve_part2(input: &(HashSet<(u16, u16)>, Vec<PaperFoldInstruction>)) -> String {
    let dots = input.1[0].apply(input.0.clone());
    let mut dots = display_dots(
        &input.1[1..]
            .iter()
            .fold(dots, |dots, paper_fold| paper_fold.apply(dots)),
    );
    dots.insert(0, '\n');
    dots
}

pub fn display_dots(dots: &HashSet<(u16, u16)>) -> String {
    let mut char_matrix = {
        let mut max_x = 0;
        let mut max_y = 0;

        for &(x, y) in dots.iter() {
            max_x = core::cmp::max(max_x, x);
            max_y = core::cmp::max(max_y, y);
        }

        vec![
            {
                let mut row = vec![' '; max_x as usize + 2];
                row[max_x as usize + 1] = '\n';
                row
            };
            max_y as usize + 1
        ]
    };

    for &(x, y) in dots.iter() {
        char_matrix[y as usize][x as usize] = '█';
    }

    char_matrix
        .into_iter()
        .flat_map(|row| row.into_iter())
        .collect::<String>()
}

impl PaperFoldInstruction {
    pub fn apply(&self, dots: HashSet<(u16, u16)>) -> HashSet<(u16, u16)> {
        match self {
            FoldAlongX(fold_line) => dots
                .into_iter()
                .filter(|(x, _)| x != fold_line)
                .map(|(x, y)| {
                    if x < *fold_line {
                        (x, y)
                    } else {
                        (fold_line + fold_line - x, y)
                    }
                })
                .collect::<HashSet<_>>(),
            FoldAlongY(fold_line) => dots
                .into_iter()
                .filter(|(_, y)| y != fold_line)
                .map(|(x, y)| {
                    if y < *fold_line {
                        (x, y)
                    } else {
                        (x, fold_line + fold_line - y)
                    }
                })
                .collect::<HashSet<_>>(),
        }
    }
}

impl core::str::FromStr for PaperFoldInstruction {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if !s.starts_with("fold along ") {
            return Err("Paper fold instruction does not start with \"fold along \"");
        }
        let s = &s[11..];

        if let Some(stripped) = s.strip_prefix("x=") {
            stripped
                .parse::<u16>()
                .map(FoldAlongX)
                .map_err(|_| "Paper fold instruction does not contain a valid number")
        } else if let Some(stripped) = s.strip_prefix("y=") {
            stripped
                .parse::<u16>()
                .map(FoldAlongY)
                .map_err(|_| "Paper fold instruction does not contain a valid number")
        } else {
            Err("Paper fold instruction does not specify \"x=\" or \"y=\"")
        }
    }
}
