use std::str::FromStr;

use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day7)]
pub fn generator_part1(input: &str) -> Vec<usize> {
    input
        .split_terminator(',')
        .map(|elem| usize::from_str(elem).unwrap())
        .collect()
}

#[aoc(day7, part1)]
pub fn solve_part1(input: &[usize]) -> usize {
    let min = input.iter().min().unwrap();
    let max = input.iter().max().unwrap();
    let (mut min_fuel_cost, mut _best_pos) = (usize::MAX, input[0]);
    for i in *min..*max {
        let x = calculate_linear_fuel_costs_for_index(input, i);
        if x < min_fuel_cost {
            min_fuel_cost = x;
            _best_pos = i;
        }
    }
    min_fuel_cost
}

#[aoc(day7, part2)]
pub fn solve_part2(input: &[usize]) -> usize {
    let min = input.iter().min().unwrap();
    let max = input.iter().max().unwrap();
    let (mut min_fuel_cost, mut _best_pos) = (usize::MAX, input[0]);
    for i in *min..*max {
        let x = calculate_non_linear_fuel_costs_for_index(input, i);
        if x < min_fuel_cost {
            min_fuel_cost = x;
            _best_pos = i;
        }
    }
    min_fuel_cost
}

pub fn calculate_linear_fuel_costs_for_index(positions: &[usize], index: usize) -> usize {
    positions.iter().fold(0, |acc, x| acc + x.abs_diff(index))
}

pub fn calculate_non_linear_fuel_costs_for_index(positions: &[usize], index: usize) -> usize {
    positions
        .iter()
        .fold(0, |acc, x| acc + nth_triangular_number(x.abs_diff(index)))
}

pub fn nth_triangular_number(n: usize) -> usize {
    (n.pow(2) + n) / 2
}

#[cfg(test)]
mod tests {
    use crate::day7::{
        calculate_linear_fuel_costs_for_index, calculate_non_linear_fuel_costs_for_index,
        generator_part1, nth_triangular_number,
    };

    #[test]
    fn that_calculating_linear_fuel_costs_works_correctly() {
        assert_eq!(
            calculate_linear_fuel_costs_for_index(&[16, 1, 2, 0, 4, 2, 7, 1, 2, 14], 2),
            37
        )
    }

    #[test]
    fn that_calculating_non_linear_fuel_costs_works_correctly() {
        assert_eq!(
            calculate_non_linear_fuel_costs_for_index(&[16, 1, 2, 0, 4, 2, 7, 1, 2, 14], 5),
            168
        )
    }

    #[test]
    fn triangular_number() {
        assert_eq!(nth_triangular_number(11), 66)
    }

    #[test]
    fn test01() {
        use crate::day7::solve_part1;

        assert_eq!(solve_part1(&generator_part1("16,1,2,0,4,2,7,1,2,14")), 2)
    }
}
