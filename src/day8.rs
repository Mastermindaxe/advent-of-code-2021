use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;

#[aoc_generator(day8, part1)]
pub fn generator_part1(input: &str) -> Vec<String> {
    input
        .lines()
        .map(|line| line.split_terminator('|'))
        .map(|mut elem| elem.nth(1).unwrap().to_string())
        .collect::<Vec<String>>()
        .iter()
        .map(|line| {
            line.split_whitespace()
                .map(|item| item.to_string())
                .collect::<Vec<String>>()
        })
        .flatten()
        .collect()
}

#[aoc(day8, part1)]
pub fn solve_part1(input: &[String]) -> usize {
    let one_count = input.iter().filter(|x| x.len() == 2).count();
    let four_count = input.iter().filter(|x| x.len() == 4).count();
    let seven_count = input.iter().filter(|x| x.len() == 3).count();
    let eight_count = input.iter().filter(|x| x.len() == 7).count();

    one_count + four_count + seven_count + eight_count
}

#[aoc_generator(day8, part2)]
pub fn generator_part2(input: &str) -> Vec<(Vec<String>, Vec<String>)> {
    let mut pair_vec: Vec<(Vec<String>, Vec<String>)> = Vec::new();
    input
        .lines()
        .map(|line| line.split_terminator('|'))
        .for_each(|mut elem| {
            elem.next().map(|string| {
                pair_vec.push((
                    string
                        .split_whitespace()
                        .map(|elem| elem.to_string())
                        .collect(),
                    Vec::new(),
                ))
            });
            elem.next().map(|string| {
                pair_vec.last_mut().unwrap().1 = string
                    .split_whitespace()
                    .map(|elem| elem.to_string())
                    .collect()
            });
        });
    pair_vec
}

pub fn solve_part2(input: Vec<(Vec<String>, Vec<String>)>) -> usize {
    input.iter().map(deduce_line).sum()
}

pub fn deduce_line(input: &(Vec<String>, Vec<String>)) -> usize {
    let one_count = input.0.iter().filter(|x| x.len() == 2).next().unwrap();
    let four_count = input.0.iter().filter(|x| x.len() == 4).next().unwrap();
    let seven_count = input.0.iter().filter(|x| x.len() == 3).next().unwrap();
    let eight_count = input.0.iter().filter(|x| x.len() == 7).next().unwrap();

    //[0, 1, 2, 3, 4, 5, 6].iter().permutations(7).unique()

    0
}

#[cfg(test)]
mod tests {
    use crate::day8::generator_part1;

    #[test]
    #[ignore]
    fn test01() {
        dbg!(generator_part1(
            "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce"
        ));
        assert!(false);
    }
}
