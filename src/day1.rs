use aoc_runner_derive::{aoc, aoc_generator};
use std::str::FromStr;

#[aoc_generator(day1)]
pub fn generator_part1(input: &str) -> Vec<isize> {
    input
        .lines()
        .map(|line| isize::from_str(line).unwrap())
        .collect()
}

#[aoc(day1, part1)]
pub fn solve_part1(input: &[isize]) -> usize {
    input.windows(2).filter(|input| input[0] < input[1]).count()
}

#[aoc(day1, part2)]
pub fn solve_part2(input: &[isize]) -> usize {
    input
        .windows(3)
        .map(|input| input[0] + input[1] + input[2])
        .collect::<Vec<isize>>()
        .windows(2)
        .filter(|input| input[0] < input[1])
        .count()
}

mod tests {
    #[test]
    fn test01() {
        use crate::day1::solve_part1;

        assert_eq!(solve_part1(&vec![0, 1, 2, 2]), 2)
    }

    #[test]
    fn test02() {
        use crate::day1::solve_part2;

        assert_eq!(
            solve_part2(&vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263]),
            5
        )
    }
}
