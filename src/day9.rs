use std::collections::{HashSet, VecDeque};
use std::iter::TrustedRandomAccessNoCoerce;

use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;

#[aoc_generator(day9)]
pub fn generator_part1(input: &str) -> Vec<Vec<usize>> {
    input
        .lines()
        .map(|line| {
            line.chars()
                .map(|char| char.to_digit(10).unwrap() as usize)
                .collect()
        })
        .collect()
}

#[aoc(day9, part1)]
pub fn solve_part1(input: &[Vec<usize>]) -> usize {
    input
        .iter()
        .enumerate()
        .map(|(y, line)| {
            line.iter()
                .enumerate()
                .filter(move |(x, _)| is_lowpoint(input, (*x, y)))
                .map(|(_, elem)| elem)
        })
        .flatten()
        .fold(0, |acc, x| acc + x + 1)
}

#[aoc(day9, part2)]
pub fn solve_part2(input: &[Vec<usize>]) -> usize {
    input
        .iter()
        .enumerate()
        .map(|(y, line)| {
            line.iter()
                .enumerate()
                .filter(move |(x, _)| is_lowpoint(input, (*x, y)))
                .map(move |(x, _)| (x, y))
        })
        .flatten()
        .map(|point| get_size(input, point))
        .sorted_unstable()
        .rev()
        .take(3)
        .product()
}

pub fn is_lowpoint(input: &[Vec<usize>], index: (usize, usize)) -> bool {
    adjacent_values(input, index)
        .iter()
        .all(|elem| elem > &input[index.1][index.0])
}

pub fn adjacent_points(input: &[Vec<usize>], index: (usize, usize)) -> Vec<(usize, usize)> {
    let upper_field = if index.1 == 0 {
        Option::None
    } else {
        Some((index.0, index.1 - 1))
    };
    let lower_field = if index.1 == input.len() - 1 {
        None
    } else {
        Some((index.0, index.1 + 1))
    };
    let left_field = if index.0 == 0 {
        Option::None
    } else {
        Some((index.0 - 1, index.1))
    };
    let right_field = if index.0 == input[0].len() - 1 {
        None
    } else {
        Some((index.0 + 1, index.1))
    };

    [upper_field, lower_field, left_field, right_field]
        .iter()
        .filter_map(|elem| *elem)
        .collect()
}

// taken from https://github.com/cs-cordero/advent-of-code/blob/master/rs/2021/day09/src/main.rs as I'm too lazy to build a flood fill algo
fn get_size(data: &[Vec<usize>], location: (usize, usize)) -> usize {
    let mut seen = HashSet::new();
    let mut queue = VecDeque::new();
    let mut size = 0;
    queue.push_back(location);

    while !queue.is_empty() {
        let (col, row) = queue.pop_front().unwrap();
        if seen.contains(&(row, col)) {
            continue;
        } else {
            seen.insert((row, col));
        }

        let value = *data.get(row).and_then(|row| row.get(col)).unwrap();
        if value == 9 {
            continue;
        }

        size += 1;
        adjacent_points(data, (col, row))
            .into_iter()
            .for_each(|point| queue.push_back(point));
    }

    size
}

fn adjacent_values(input: &[Vec<usize>], index: (usize, usize)) -> Vec<usize> {
    adjacent_points(input, index)
        .into_iter()
        .map(|(x, y)| input[y][x])
        .collect()
}

#[cfg(test)]
mod tests {
    use crate::day9::generator_part1;

    #[test]
    fn test01() {
        use crate::day9::solve_part1;

        assert_eq!(
            solve_part1(&generator_part1(
                "2199943210
3987894921
9856789892
8767896789
9899965678"
            )),
            15
        )
    }

    #[test]
    fn test02() {
        use crate::day9::solve_part2;

        assert_eq!(
            solve_part2(&generator_part1(
                "2199943210
3987894921
9856789892
8767896789
9899965678"
            )),
            1134
        )
    }
}
