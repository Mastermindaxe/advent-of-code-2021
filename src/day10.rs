use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;
use std::str::FromStr;

#[aoc_generator(day10)]
pub fn generator_part1(input: &str) -> Vec<String> {
    input.lines().map(|line| line.to_string()).collect()
}

#[aoc(day10, part1)]
pub fn solve_part1(input: &[String]) -> usize {
    let valid_opening = ['(', '[', '{', '<'];
    let mut corrupted_at_chars = Vec::new();

    let mut stack = Vec::new();
    for line in input {
        let mut line_corrupted_at = None;
        line.chars().for_each(|char| {
            if valid_opening.contains(&char) {
                stack.push(char);
            } else if char == ')' {
                if let Some(popped) = stack.pop() {
                    if popped != '(' {
                        line_corrupted_at = Some(')');
                    }
                } else {
                    unimplemented!()
                }
            } else if char == ']' {
                if let Some(popped) = stack.pop() {
                    if popped != '[' {
                        line_corrupted_at = Some(']');
                    }
                } else {
                    unimplemented!()
                }
            } else if char == '}' {
                if let Some(popped) = stack.pop() {
                    if popped != '{' {
                        line_corrupted_at = Some('}');
                    }
                } else {
                    unimplemented!()
                }
            } else if char == '>' {
                if let Some(popped) = stack.pop() {
                    if popped != '<' {
                        line_corrupted_at = Some('>');
                    }
                } else {
                    unimplemented!()
                }
            }
        });
        if let Some(corrupted) = line_corrupted_at {
            corrupted_at_chars.push(corrupted)
        }
    }

    corrupted_at_chars.iter().fold(0, |acc, char| {
        acc + match char {
            ')' => 3,
            ']' => 57,
            '}' => 1197,
            '>' => 25137,
            _ => unimplemented!(),
        }
    })
}

#[aoc(day10, part2)]
pub fn solve_part2(input: &[String]) -> usize {
    let valid_opening = ['(', '[', '{', '<'];

    let mut remaining_lines = Vec::new();
    let mut closing_sequences = Vec::new();
    for line in input {
        let mut stack = Vec::new();
        let mut line_corrupted_at = None;
        line.chars().for_each(|char| {
            if valid_opening.contains(&char) {
                stack.push(char);
            } else if char == ')' {
                if let Some(popped) = stack.pop() {
                    if popped != '(' {
                        line_corrupted_at = Some(')');
                    }
                } else {
                    unimplemented!()
                }
            } else if char == ']' {
                if let Some(popped) = stack.pop() {
                    if popped != '[' {
                        line_corrupted_at = Some(']');
                    }
                } else {
                    unimplemented!()
                }
            } else if char == '}' {
                if let Some(popped) = stack.pop() {
                    if popped != '{' {
                        line_corrupted_at = Some('}');
                    }
                } else {
                    unimplemented!()
                }
            } else if char == '>' {
                if let Some(popped) = stack.pop() {
                    if popped != '<' {
                        line_corrupted_at = Some('>');
                    }
                } else {
                    unimplemented!()
                }
            }
        });
        if line_corrupted_at.is_none() {
            remaining_lines.push(line);
            stack.reverse();
            closing_sequences.push(stack);
        }
    }

    closing_sequences
        .iter()
        .map(calculate_score_for_line)
        .sorted()
        .nth(closing_sequences.len() / 2)
        .unwrap()
}

pub fn calculate_score_for_line(line: &Vec<char>) -> usize {
    line.iter().fold(0, |acc, char| {
        acc * 5
            + match char {
                '(' => 1,
                '[' => 2,
                '{' => 3,
                '<' => 4,
                _ => unimplemented!(),
            }
    })
}

#[cfg(test)]
mod tests {
    use crate::day10::generator_part1;

    #[test]
    fn test01() {
        use crate::day10::solve_part1;

        assert_eq!(
            solve_part1(&generator_part1(
                "[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]"
            )),
            26397
        )
    }

    #[test]
    fn test02() {
        use crate::day10::solve_part2;

        assert_eq!(
            solve_part2(&generator_part1(
                "[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]"
            )),
            288957
        )
    }
}
