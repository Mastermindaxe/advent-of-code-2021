use std::collections::HashMap;
use std::str::FromStr;

use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day6)]
pub fn generator_part1(input: &str) -> HashMap<usize, u128> {
    let mut map = HashMap::new();
    input.split_terminator(',').for_each(|elem| {
        map.entry(usize::from_str(elem).unwrap())
            .and_modify(|elem| *elem += 1)
            .or_insert(1);
    });
    map
}

#[aoc(day6, part1)]
pub fn solve_part1(input: &HashMap<usize, u128>) -> u128 {
    solve_by_days(input, 80)
}

#[aoc(day6, part2)]
pub fn solve_part2(input: &HashMap<usize, u128>) -> u128 {
    solve_by_days(input, 256)
}

pub fn solve_by_days(input: &HashMap<usize, u128>, days: usize) -> u128 {
    let mut result = input.clone();
    for _ in 0..days {
        let mut new_map = result.clone();

        result.iter().for_each(|(key, value)| match key {
            0 => {
                new_map
                    .entry(6)
                    .and_modify(|elem| *elem += value)
                    .or_insert(*value);
                new_map
                    .entry(8)
                    .and_modify(|elem| *elem += value)
                    .or_insert(*value);
                new_map.entry(0).and_modify(|elem| *elem -= value);
            }
            _ => {
                new_map.entry(*key).and_modify(|elem| *elem -= value);
                new_map
                    .entry(*key - 1)
                    .and_modify(|elem| *elem += value)
                    .or_insert(*value);
            }
        });
        result = new_map;
    }
    result.iter().fold(0_u128, |acc, x| x.1 + acc)
}

#[cfg(test)]
mod tests {
    use crate::day6::generator_part1;

    #[test]
    fn test01() {
        use crate::day6::solve_part1;

        assert_eq!(solve_part1(&generator_part1("3,4,3,1,2")), 5934)
    }

    #[test]
    fn test02() {
        use crate::day6::solve_part2;

        assert_eq!(solve_part2(&generator_part1("3,4,3,1,2")), 26984457539)
    }
}
